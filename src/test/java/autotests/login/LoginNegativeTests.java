package autotests.login;

import autotests.common.TestBase;
import autotests.helpers.TestHelper;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages.LoginPage;

import static autotests.config.ConfProperties.getConfProperties;
import static org.testng.Assert.*;


public class LoginNegativeTests extends TestBase {

    LoginPage page;

    @BeforeMethod
    void setupLoginTest() {
        page = new LoginPage();
    }

    @Test
    @Description("Тест авторизации с некорректными данными")
    public void incorrectUserNameAndPassword() {
        String login = "TestUser";
        String passwd = "Password";

        page.inputLogin(login)
                .inputPass(passwd);

        // проверить, что до нажатия кнопки "Войти" на странице нет текста Invalid credentials.
        assertFalse(page.checkInvalidCredTextNotNull());

        page.clickLogin();
        // проверить, что текст Invalid credentials. появился на странице
        assertTrue(page.checkInvalidCredTextNotNull());

        SoftAssert soft = new SoftAssert();

        soft.assertEquals(page.getTextLogin(), login);
        soft.assertEquals(page.getTextPass(), "");
        soft.assertAll();
    }

    @Test
    @Description("Тест авторизации с пустыми полями")
    public void notRedirectUserNameAndPassword() {
        TestHelper.authorize(page, "", "");

        assertFalse(page.checkInvalidCredTextNotNull());

        String URL = WebDriverRunner.url();
        assertEquals(URL, getConfProperties().getStartpage() + "/login" );
    }

}