package autotests.login;

import autotests.common.TestBase;
import autotests.helpers.TestHelper;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.codeborne.selenide.testng.ScreenShooter;
import io.qameta.allure.Description;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.EditPage;
import pages.LoginPage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static autotests.config.ConfProperties.getConfProperties;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Listeners({ScreenShooter.class})
public class LoginPositiveTests extends TestBase {
    LoginPage page;
    EditPage editPage;

    @BeforeMethod
    void setupLoginTest() {
        page = new LoginPage();
        editPage = new EditPage();
    }

    @Test
    @Description("Проверка возможности изменять размер окна браузера")
    public void initWebDriver() throws InterruptedException {
        WebDriver driver = WebDriverRunner.getWebDriver();
        //2. Размер окна меняется на 200*100
        driver.manage().window().setSize(new Dimension(200, 100));
        Thread.sleep(5000);

        //3. Окно разворачивается на полный экран
        driver.manage().window().maximize();
        Thread.sleep(5000);
    }

    public static byte[] getBytes(String resourceName) throws IOException {
        return Files.readAllBytes(Paths.get("", resourceName));
    }

    @Test(dataProvider = "arguments")
    @Description("Тест авторизации с корректными данными")
    public void correctUserNameAndPassword(String username, String password) throws InterruptedException, IOException {
        TestHelper.authorize(page, username, password);

        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));
        String URL = WebDriverRunner.url();

        assertEquals( getConfProperties().getStartpage() + "/report/group/edit", URL, "Не удалось авторизоваться, проверьте логин и пароль");

        //Нажать на аватар в верхнем правом углу
        editPage.clickAvatar();

        //Фамилия пользователя соответствует ожидаемой
        String[] words = username.split(" ");
        if (words.length > 1) {
            assertTrue(editPage.checkFio(words[1]));
        }

        //Email пользователя равен: “fake+ИД@quality-lab.ru”
        assertTrue(editPage.checkEmail("fake+190@quality-lab.ru"));
    }

    @DataProvider(name = "arguments")
    static  Object[][] argumentsProvider() {
        return  new Object[][] {
                {"Тест", "Тест"},
                {"Светлана Кузина", "Panterka_S13"}
        };
    }

}