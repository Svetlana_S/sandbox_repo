package autotests.calendar;

import autotests.helpers.TestHelper;
import autotests.common.TestBase;
import io.qameta.allure.Description;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.CalendarPage;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static autotests.config.ConfProperties.getConfProperties;
import static com.codeborne.selenide.Selenide.open;
import static org.testng.Assert.assertEquals;

public class CalendarTest extends TestBase {
    private CalendarPage calendarPage;

    @BeforeMethod
    public void setupCalendarTest() throws IOException {
        calendarPage = new CalendarPage();
        TestHelper.authorizeByHttp();
        open(getConfProperties().getStartpage() +"/calendar/");
        // дождаться полной загрузки
        calendarPage.waitOverlay();
        calendarPage.waitOverlayDisappear();
    }

    @Test
    @Description("Проверка текущего месяца - месяц и год совпадают с текущими")
    public void checkMonth() {
        String nowDate = LocalDate.now().format(DateTimeFormatter.ofPattern("LLLL yyyy"));
        assertEquals(nowDate.toLowerCase(), calendarPage.getMonthAndYear());

        calendarPage.checkDays();
    }

    //Проверка переключения месяца
    @Test
    @Description("Проверка переключения месяца")
    public void checkNextMonth() {
        //Название месяца из 3 букв
        String nextMonthStr = LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern("MMM"));
        //Это чтобы первая буква была заглавной
        String capNextMonth = nextMonthStr.substring(0, 1).toUpperCase() + nextMonthStr.substring(1);

        calendarPage.clickNextMonth(capNextMonth);
        calendarPage.checkDays();
    }

    //Проверка графика работы другого сотрудника
    @Test
    @Description("Проверка графика работы другого сотрудника")
    public void checkAnotherEmployeeMonth() {
        calendarPage.setAnotherUser("Агеева")
                .clickBtnSend();
        calendarPage.checkDays();
    }

}
