package autotests.holiday;

import autotests.config.ConfProperties;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static org.testng.Assert.assertEquals;

public class GetHolidaysTest {
    private final int holyDayType = 2;
    private final int shortDayType = 1;

    @BeforeMethod
    public static void setup() {
        baseURI = ConfProperties.getConfProperties().getStartpage();
        port = 443;
        basePath = "/api/v2/public";

        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.addParam("key", "wvS9fmlcgT6jOIO6tyhESV55F6dbNpk3PeWkobf8");
        requestSpecification = requestSpecBuilder.build();

        ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
        responseSpecBuilder.expectStatusCode(200);
        responseSpecBuilder.expectBody("response.message.type", not(hasItem("error")));
        responseSpecification = responseSpecBuilder.build();
    }

    @Test(priority = 1)
    @Description("Тест получения выходных и праздничных дней")
    public void withoutParametersTest() {
        JsonPath jsonPath = when()
                .get("/Calendar/GetHolidays")
                .then()
                .body("response.items.date", everyItem(startsWith(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy")))))
                .extract()
                .body()
                .jsonPath();

        checkAllDays(jsonPath, LocalDate.now().getYear());
    }

    @Test(priority = 2)
    @Description("Тест получения дней только типа SHORT_DAY за текущий год")
    public void getSHORT_DAYTest() {
        setDayTypeToRequest("short_day");
        setDayTypeToResponse("short_day");

        JsonPath jsonPath = when()
                .get("/Calendar/GetHolidays")
                .then()
                .spec(responseSpecification)
                .body("response.items.date", everyItem(startsWith(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy")))))
                .extract()
                .body()
                .jsonPath();

        SoftAssert softAssert = new SoftAssert();
        List<HolidayItem> items = jsonPath.getList("response.items", HolidayItem.class);
        checkYear(softAssert, items, LocalDate.now().getYear());

        softAssert.assertFalse(searchItemByType(items, holyDayType), "Найдены дни типа HOLY_DAY");
        softAssert.assertTrue(searchItemByType(items, shortDayType), "Не найдено дней типа SHORT_DAY");
        softAssert.assertAll();
    }


    @Test(priority = 4)
    @Description("Тест получения выходных и праздничных дней за 2019 год")
    public void getHolidays2019Test() {
        int year = 2019;
        setYearToRequest(year);
        setDayTypeToRequest("");

        JsonPath jsonPath = when()
                .get("/Calendar/GetHolidays")
                .then()
                .body("response.items.date", everyItem(startsWith(String.valueOf(year))))
                .extract()
                .body()
                .jsonPath();

        checkAllDays(jsonPath, year);
    }

    @Test(priority = 3)
    @Description("Тест получения дней только типа HOLY_DAY за текущий год")
    public void getHOLY_DAYTest() {
        setDayTypeToRequest("holy_day");
        setDayTypeToResponse("holy_day");

        JsonPath jsonPath = when()
                .get("/Calendar/GetHolidays")
                .then()
                .spec(responseSpecification)
                .body("response.items.date", everyItem(startsWith(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy")))))
                .extract()
                .jsonPath();

        SoftAssert softAssert = new SoftAssert();

        List<HolidayItem> items = jsonPath.getList("response.items", HolidayItem.class);
        checkYear(softAssert, items, LocalDate.now().getYear());

        softAssert.assertTrue(searchItemByType(items, holyDayType), "Не найдено дней типа HOLY_DAY");
        softAssert.assertFalse(searchItemByType(items, shortDayType), "Найдены дни типа SHORT_DAY");
        softAssert.assertAll();

    }

    @Test(priority = 6)
    @Description("Тест получения выходных и праздничных дней за 1987 год")
    public void getWORK_DAYTest() {
        int year = 1987;
        setYearToRequest(year);

        JsonPath jsonPath = when()
                .get("/Calendar/GetHolidays")
                .then()
                .body("response.items.date", everyItem(startsWith(String.valueOf(year))))
                .extract()
                .body()
                .jsonPath();

        List<HolidayItem> items = jsonPath.getList("response.items", HolidayItem.class);
        assertEquals(items.size(), 0);
    }

    @Test(priority = 5)
    @Description("Тест получения дней типа work_day")
    public void setWrongDayTypeTest() {

        setDayTypeToRequest("work_day");
        JsonPath jsonPath = when()
                .get("/Calendar/GetHolidays")
                .body()
                .jsonPath();

        List<HolidayItem> items = jsonPath.getList("response.items", HolidayItem.class);
        assertEquals(items.size(), 0);
    }

    private void setDayTypeToRequest(String dayType) {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.addParam("day_type", dayType);
        requestSpecification = requestSpecBuilder.build();
    }

    private void setDayTypeToResponse(String type) {
        ResponseSpecBuilder responseSpecHOLY_DAY = new ResponseSpecBuilder();
        responseSpecHOLY_DAY.expectBody("response.items.type", hasItem(type));
        responseSpecification = responseSpecHOLY_DAY.build();
    }

    private void setYearToRequest(int year) {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.addParam("year", year);
        requestSpecification = requestSpecBuilder.build();
    }

    private void checkYear(SoftAssert softAssert, List<HolidayItem> items, int year) {
        items.forEach(item -> {
                    softAssert.assertEquals(item.getDate().getYear(), year, "Найден день другого года: " + item + " \nИскомый год: " + year);
                });
    }

    private boolean searchItemByType(List<HolidayItem> items, int type_id) {
        return items.stream().anyMatch(item -> (item.getType_id() == type_id));
    }

    @Step("Проверка наличия выходных и праздничных дней")
    private void checkAllDays(JsonPath jsonPath, int year) {
        SoftAssert softAssert = new SoftAssert();
        List<HolidayItem> items = jsonPath.getList("response.items", HolidayItem.class);

        checkYear(softAssert, items, year);

        softAssert.assertTrue(searchItemByType(items, holyDayType), "Не найдено дней типа HOLY_DAY");
        softAssert.assertTrue(searchItemByType(items, shortDayType), "Не найдено дней типа SHORT_DAY");
        softAssert.assertAll();
    }

}
