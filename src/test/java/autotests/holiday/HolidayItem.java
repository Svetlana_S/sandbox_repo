package autotests.holiday;

import java.time.LocalDate;

public class HolidayItem {
    private LocalDate date;
    private String type;
    private int type_id;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = LocalDate.parse(date);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String toString() {
        return "Date: " + date.toString() + " Type: " + type + " Type_id: " + type_id;
    }
}
