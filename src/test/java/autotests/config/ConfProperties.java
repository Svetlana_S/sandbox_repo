package autotests.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class ConfProperties {
    protected InputStream fileStream;
    protected Properties PROPERTIES;

    private String name = "/conf.properties";
    private static ConfProperties instance;

    private ConfProperties() {
        try {
            //указание пути до файла с настройками
            fileStream = ConfProperties.class.getResourceAsStream(name);
            try (Reader reader = new InputStreamReader(fileStream, StandardCharsets.UTF_8)) {
                PROPERTIES = new Properties();
                PROPERTIES.load(reader);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileStream != null)
                try {
                    fileStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    public static ConfProperties getConfProperties() {
        if (instance == null) {
            instance = new ConfProperties();
        }
        return instance;
    }

    public String getStartpage() {
        if (System.getenv("URL") != null)
            return System.getenv("URL");
        else
            return PROPERTIES.getProperty("startpage");
    }

    public String getUsername() {
        return PROPERTIES.getProperty("login");
    }

    public String getPassword() {
        return PROPERTIES.getProperty("password");
    }

    public void setStartpage(String urlAddr) {
        PROPERTIES.setProperty("startpage", urlAddr);
    }

    public void setUsername(String username) {
        PROPERTIES.setProperty("login", username);
    }

    public void setPassword(String password) {
        PROPERTIES.setProperty("password", password);
    }
}
