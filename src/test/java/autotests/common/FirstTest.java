package autotests.common;


import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class FirstTest {
    int x = 2+2;

    @Test
    void myTest() {
        System.out.println("My first autotest running");
        int y = 1/0;
        assertEquals(y, 1);
    }

    @Test
    void myTest1() {
        System.out.println("Test #1");
        assertEquals(x, 4);
    }

    @Test
    void myTest2() {
        System.out.println("Test #2");
        assertEquals(x, 5);
    }

    @Test
    void myTest3() {
        System.out.println("Test #3");
        assertTrue(x == 4);
    }

    @Test
    void myTest4() {
        System.out.println("Test #4");
        assertTrue(x == 5);
    }

    @Test
    void myTest5() {
        System.out.println("Test #5");
        SoftAssert soft = new SoftAssert();
        soft.assertEquals(4, x);
        soft.assertEquals(5, x);
        soft.assertTrue(x == 4, "");
        soft.assertTrue(x == 5, "");
        soft.assertAll();

    }

    @BeforeMethod
    private void printLogStart() {
        System.out.println("Test start");
    }

    @AfterMethod
    private void printLogEnd() {
        System.out.println("Test finished");
    }

    @BeforeClass
    private static void printLogTestClassStart() {
        System.out.println("autotests.login.FirstTest class started");
    }

    @AfterClass
    private static void printLogTestClassEnd() {
        System.out.println("All tests in autotests.login.FirstTest finished");
    }
}
