package autotests.common;

import autotests.helpers.MyTestListener;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

@Listeners(MyTestListener.class)
public class TestBase {

    @BeforeClass
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

}
