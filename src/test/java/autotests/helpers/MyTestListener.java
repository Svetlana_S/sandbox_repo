package autotests.helpers;

import io.qameta.allure.Allure;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.codeborne.selenide.Selenide.screenshot;

public class MyTestListener implements ITestListener {

    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        String pngFileName = screenshot("testFail"); //получается адрес типа file:/C:/Users....

        //прикрепляю скриншот к отчету
        String bfile = null;
        if (pngFileName != null) {
            bfile = pngFileName.replaceAll("file:/", "");
        }

        byte[] myImage = new byte[0];

        try {
            myImage = getBytes(bfile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayInputStream imageInputStream = new ByteArrayInputStream(myImage);
        Allure.addAttachment("testFail", "image/png", imageInputStream, "png");

        System.out.println("Test failed");
        System.out.println("Test failed");
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }

    public static byte[] getBytes(String resourceName) throws IOException {
        return Files.readAllBytes(Paths.get("", resourceName));
    }
}
