package autotests.helpers;

import autotests.common.TestBase;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Step;
import okhttp3.*;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import pages.LoginPage;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;

import static autotests.config.ConfProperties.getConfProperties;
import static com.codeborne.selenide.Selenide.open;

public class TestHelper extends TestBase {
    private static LoginPage page;
    private String username = getConfProperties().getUsername();
    private String password = getConfProperties().getPassword();
    private static CookieManager cookieManager;
    private static OkHttpClient client;

    public TestHelper() {

    }

    @Step("Авторизация")
    public void authorize() {
        page = new LoginPage();
        page.inputLogin(username)
                .inputPass(password)
                .clickLogin();
    }

    @Step("Авторизация")
    public static void authorize(LoginPage pageLogin, String username, String password) {
        open(getConfProperties().getStartpage());
        pageLogin.inputLogin(username)
                .inputPass(password)
                .clickLogin();
    }

    @Step("Авторизация через http-запрос")
    public static void authorizeByHttp() throws IOException {
        // создали куки-менеджера
        createCookie();

        // открыли сайт
        firstOpenLogin_authorize();

        //отправили запрос авторизации
        //после успешной авторизации получили куки
        sendPostRequest_authorize();

        open(getConfProperties().getStartpage());
        WebDriver driver = WebDriverRunner.getWebDriver();

        //новые куки передали в браузер, на всякий случай почистив куки браузера
        driver.manage().deleteAllCookies();

        cookieManager.getCookieStore().getCookies().forEach(httpCookie -> {
            Cookie.Builder builder = new Cookie.Builder(httpCookie.getName(), httpCookie.getValue());
            builder.domain(httpCookie.getDomain());
            builder.path(httpCookie.getPath());
            Cookie cookie = builder.build();
            driver.manage()
                    .addCookie(cookie);
        });

        driver.navigate().refresh();
    }

    public static void createCookie() {
        cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        JavaNetCookieJar cookieJar = new JavaNetCookieJar(cookieManager);

        client = new OkHttpClient.Builder()
                .cookieJar(cookieJar)
                .followRedirects(false)
                .build();

    }

    public static void sendPostRequest_authorize() throws IOException {
        RequestBody formBody = new FormBody.Builder()
                .add("_username", "Светлана Кузина")
                .add("_password", "Panterka_S13")
                .add("_csrf_token", "")
                .add("_submit", "Войти")
                .build();

        Headers headers = new Headers.Builder()
                .add("Content-Type","application/x-www-form-urlencoded")
                .add("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8")
                .build();

        Request request = new Request.Builder()
                .url(getConfProperties().getStartpage() + "/login_check")
                .post(formBody)
                .headers(headers)
                .build();

        Call call = client.newCall(request);
        call.execute();

    }

    public static void firstOpenLogin_authorize() throws IOException {
        Request request = new Request.Builder()
                .url(getConfProperties().getStartpage() + "/login")
                .build();

        Call call = client.newCall(request);
        call.execute();

    }

}
