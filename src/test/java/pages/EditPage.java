package pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class EditPage {

    private SelenideElement avatar = $(By.className("avatarCover"));
    private SelenideElement fio = $(By.className("m-card-user__name"));
    private SelenideElement email = $(By.className("m-card-user__email"));

    public EditPage() {    }

    @Step("Нажатие на аватар")
    public void clickAvatar() {
        avatar.click();
    }

    @Step("Проверка ФИО")
    public boolean checkFio(String fioText) {
        return fio.getText().contains(fioText);
    }

    @Step("Проверка e-mail")
    public boolean checkEmail(String emailText) {
        return email.getText().contains(emailText);
    }

}
