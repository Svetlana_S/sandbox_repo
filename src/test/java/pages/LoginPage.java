package pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class LoginPage {
    private TextInput loginField = new TextInput($("#username"));
    private TextInput passField = new TextInput($("#password"));
    private Button btnLogin = new Button($("[value=Войти]"));

    private SelenideElement invalidText = null;
    private String invCredText = "Invalid credentials.";

    public LoginPage() {   }

    @Step("Ввод логина")
    public LoginPage inputLogin(String login) {
        loginField.clear();
        loginField.sendKeys(login);
        return this;
    }

    @Step("Ввод пароля")
    public LoginPage inputPass(String passwd) {
        passField.sendKeys(passwd);
        return this;
    }

    @Step("Получение текста из поля Имя пользователя")
    public String getTextLogin() {
        return loginField.getAttribute("value");
    }

    @Step("Получение текста из поля Пароль")
    public String getTextPass() {
        return passField.getAttribute("value");
    }

    @Step("Нажатие на кнопку Войти")
    public EditPage clickLogin() {
        btnLogin.click();
        return new EditPage();
    }

    @Step("Проверка текста Invalid credentials.")
    public boolean checkInvalidCredTextNotNull() {
        invalidText = $(byText(invCredText));
        return invalidText.exists();
    }
}
