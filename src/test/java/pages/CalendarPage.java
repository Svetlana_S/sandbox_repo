package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import ru.yandex.qatools.htmlelements.element.Button;

import java.util.concurrent.atomic.AtomicReference;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.withText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.testng.Assert.assertTrue;

public class CalendarPage {
    //плашка ожидания загрузки
    private String calendarUpdateText = "Обновление календаря";
    private SelenideElement overlay = $(byText(calendarUpdateText));

    private SelenideElement monthAndYear = $("#schedule-month-title");
    private SelenideElement userListBtn = $(".col-lg-3.col-md-12");

    private SelenideElement userListDropdown = $(By.className("//*[contains(text(), 'Сотрудник')]/ancestor::div/following-sibling::span[contains(@class,'select2-container')]"));
    private Button sendBtn = new Button($(".btn_do_filter"));
    private SelenideElement calendarDropdownBtn = $(".la-calendar-check-o");
    private SelenideElement monthsDropdown = $(".datepicker-months");

    private String workDayClass = "schedule-badge--default";
    private String weekendDayClass = "schedule-badge--no-event";

    public CalendarPage() { }

    @Step("Получение месяца и года с календаря")
    public String getMonthAndYear() {
        return monthAndYear.getText();
    }

    @Step("Получение коллекции элементов месяца")
    private ElementsCollection getDaysFromCalendar() {
        return $$("a.schedule-badge");
    }

    @Step("Выбор другого пользователя из выпадающего списка")
    public CalendarPage setAnotherUser(String username) {
        userListBtn.click();
        //ожидание выпадающего списка
        userListDropdown.shouldBe(Condition.visible);
        userListDropdown.find(withText(username)).click();
        return this;
    }

    @Step("Нажатие кнопки Применить")
    public CalendarPage clickBtnSend() {
        sendBtn.click();
        return this;
    }

    @Step("Ожидание загрузки календаря")
    public void waitOverlay() {
        if (!overlay.exists()) {
            Selenide.Wait().until(webDriver -> {
                return overlay.getText().equals(calendarUpdateText);
            });
        }
    }

    @Step("Ожидание исчезновения плашки \"Обновление календаря\" ")
    public void waitOverlayDisappear() {
        overlay.shouldBe(Condition.disappear);
    }

    @Step("Нажатие на следующий месяц")
    public void clickNextMonth(String nextMonthStr) {
        calendarDropdownBtn.click();
        monthsDropdown.shouldBe(Condition.visible);
        getMonthLocator(nextMonthStr).click();
    }

    private SelenideElement getMonthLocator(String nextMonthStr) {
        return $(By.xpath("//div[@class='datepicker-months']//*[contains(text(), '" + nextMonthStr +"')]"));
    }

    @Step("Проверка наличия и рабочих, и выходных дней в месяце")
    public void checkDays() {
        AtomicReference<Boolean> weekendExists = new AtomicReference<>(false);
        AtomicReference<Boolean> workDaysExists = new AtomicReference<>(false);

        ElementsCollection days = getDaysFromCalendar();
        days.stream().forEach(day -> {
            String dayCls = day.getAttribute("class");
            if (dayCls != null) {
                if (dayCls.contains(workDayClass)) {
                    workDaysExists.set(true);
                } else if (dayCls.contains(weekendDayClass)) {
                    weekendExists.set(true);
                }
            }
        });

        assertTrue(workDaysExists.get());
        assertTrue(weekendExists.get());
    }
}
